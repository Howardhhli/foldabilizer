1. Print the hinge_tester.obj in its original size, which is around 10cm x 10cm x 4cm. 
2. After printing, try to fold the modle into 3-layered configuration via two hinges.

3. If the printed model in Step 2 fails to fold, try to print the shape in the doubled size. 
4. Repeat Step 2. 

Please contact me via email: 
howard.hhli@gmail.com

Thanks for helping out the printing. 

--
Honghua Li