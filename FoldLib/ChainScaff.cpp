#include "ChainScaff.h"
#include "FdUtility.h"
#include "RodNode.h"
#include "Numeric.h"
#include "ParSingleton.h"
#include "DistSegRect.h"

ChainScaff::ChainScaff( ScaffNode* slave, PatchNode* base, PatchNode* top)
	: Scaffold(slave->mID)
{
	// masters, slave, and thickness
	setupMasters(base, top);
	setupSlave(slave);
	setupThickness();

	// basic orientations
	computeBasicOrientations();
	computeRightDirection();

	// debug
	//showBasicOrientations();

	// fold parameters
	duration.set(1, 2);
	foldToRight = true;
	isDeleted = false;
	importance = 0;
}

void ChainScaff::setupMasters(PatchNode* base, PatchNode* top)
{
	baseMaster = (PatchNode*)base->clone();
	topMaster = (PatchNode*)top->clone();
	Graph::addNode(baseMaster);
	Graph::addNode(topMaster);

	// keep the original top center
	topCenterOrig = topMaster->center();

	//debug
	//visDebug.addRectangle(baseMaster->mPatch);
}

void ChainScaff::setupSlave(ScaffNode* slave)
{
	// clone
	if (slave->mType == ScaffNode::ROD)
	{
		// convert slave into patch if need
		RodNode* slaveRod = (RodNode*)slave;
		Vector3 rodV = slaveRod->mRod.Direction;
		Vector3 baseV = baseMaster->mPatch.Normal;
		Vector3 crossRodVBaseV = cross(rodV, baseV);
		Vector3 slavePatchNorm;
		if (crossRodVBaseV.norm() < 0.1)
			slavePatchNorm = baseMaster->mPatch.Axis[0];
		else slavePatchNorm = cross(crossRodVBaseV, rodV);
		slavePatchNorm.normalize();

		origSlave = new PatchNode(slaveRod, slavePatchNorm);
	}
	else{
		origSlave = (PatchNode*)slave->clone();
	}

	// deform to attach masters perfectly
	if (ParSingleton::instance()->useThk)
	{
		origSlave->deformToAttach(baseMaster->getSurfacePlane(true)); // the top of base master
		if (!topMaster->hasTag(EDGE_VIRTUAL_TAG)) // skip top virtual for T-chain
			origSlave->deformToAttach(topMaster->getSurfacePlane(false)); // the bottom of top master
	}else
	{
		origSlave->deformToAttach(baseMaster->mPatch.getPlane()); // the skeletal patch
		if (!topMaster->hasTag(EDGE_VIRTUAL_TAG)) // skip top virtual for T-chain
			origSlave->deformToAttach(topMaster->mPatch.getPlane()); // the skeletal patch
	}

	// save parts
	chainParts << (PatchNode*)origSlave->clone();
	Structure::Graph::addNode(chainParts[0]);
}

void ChainScaff::setupThickness()
{
	topHThk = topMaster->getThickness() / 2;
	baseHThk = baseMaster->getThickness() / 2;
	slaveHThk = origSlave->getThickness() / 2;
}

void ChainScaff::computeBasicOrientations()
{
	// the top and base patch edges 
	QVector<Geom::Segment> perpEdges = origSlave->mPatch.getPerpEdges(baseMaster->mPatch.Normal);
	Geom::DistSegRect dsr0(perpEdges[0], baseMaster->mPatch);
	Geom::DistSegRect dsr1(perpEdges[1], baseMaster->mPatch);
	Geom::Segment topEdge = perpEdges[0];
	Geom::Segment baseEdge = perpEdges[1];
	if (dsr0.get() < dsr1.get()) {
		baseEdge = perpEdges[0]; topEdge = perpEdges[1];
	}
	if (dot(topEdge.Direction, baseEdge.Direction) < 0) topEdge.flip();

	// the up right segment
	Geom::Rectangle baseSurface = baseMaster->getSurfaceRect(true);
	Geom::Segment topEdgeProj = baseSurface.getProjection(topEdge);
	upSeg.set(topEdgeProj.Center, topEdge.Center);

	// slaveSeg : bottom to up
	slaveSeg.set(baseEdge.Center, topEdge.Center);

	// the joints
	baseJoint = baseEdge;
	topJoint = topEdge;

	// thickness
	if (ParSingleton::instance()->useThk)
	{
		// trim the ends for ball joints
		double topTrimRatio = slaveHThk / upSeg.length();
		double baseTrimRatio = slaveHThk / upSeg.length();
		if (topMaster->hasTag(EDGE_VIRTUAL_TAG)) topTrimRatio = 0;

		upSeg.cropRange01(baseTrimRatio, 1 - topTrimRatio);

		slaveSeg.cropRange01(baseTrimRatio, 1 - topTrimRatio);

		baseJoint.translate(slaveSeg.P0 - baseJoint.Center);
		topJoint.translate(slaveSeg.P1 - topJoint.Center);
	}
}

void ChainScaff::computeRightDirection()
{
	// get the base rect
	Geom::Rectangle baseRect;
	if (ParSingleton::instance()->useThk){
		baseRect = baseMaster->getSurfaceRect(true);
		baseRect.translate(slaveHThk * upSeg.Direction);
	}else
		baseRect = baseMaster->mPatch;

	// right segment and right direction
	Vector3 topCentreProj = baseRect.getProjection(topJoint.Center);
	rightSeg.set(topCentreProj, baseJoint.Center);

	// almost vertical: around 5 degrees tolerance
	if (rightSeg.length() / slaveSeg.length() < 0.1)
	{
		// baseJoint, slaveSeg and rightDirect are right-handed
		rightDirect = cross(baseJoint.Direction, slaveSeg.Direction);
		rightDirect = baseRect.getProjectedVector(rightDirect);
		rightDirect.normalize();
	}else
	{
		// otherwise
		rightDirect = rightSeg.Direction;
		Vector3 crossSlaveRight = cross(slaveSeg.Direction, rightDirect);
		if (dot(crossSlaveRight, baseJoint.Direction) < 0){
			baseJoint.flip(); topJoint.flip();
		}
	}

	// flip slave patch so that its norm is to the right
	if (dot(origSlave->mPatch.Normal, rightDirect) < 0)
		origSlave->mPatch.flipNormal();
}

ChainScaff::~ChainScaff()
{
	delete origSlave;
}

// the keyframe cannot be nullptr
Scaffold* ChainScaff::getKeyframe( double t )
{
	Scaffold* keyframe = nullptr;

	// deleted chain only has the base master
	if (isDeleted)
	{
		keyframe = new Scaffold();
		keyframe->Structure::Graph::addNode(baseMaster->clone());
		return keyframe;
	}
	// otherwise fold the chain and clone
	else
	{
		fold(t);
		keyframe = (Scaffold*)this->clone();
	}

	return keyframe;
}

void ChainScaff::setFoldDuration( double t0, double t1 )
{
	if (t0 > t1) std::swap(t0, t1);
	t0 += ZERO_TOLERANCE_LOW;
	t1 -= ZERO_TOLERANCE_LOW; 

	duration.set(t0, t1);
}

FoldOption* ChainScaff::genFoldOption(QString id, bool isRight, double width, double startPos, int nSplits)
{
	FoldOption* fn = new FoldOption(id, isRight, width, startPos, nSplits);
	fn->region = getFoldRegion(fn);
	fn->duration = duration;

	return fn;
}

QVector<FoldOption*> ChainScaff::genRegularFoldOptions( int nSplits, int nChunks )
{
	int maxNbChunks = ParSingleton::instance()->maxNbChunks;
	double chunkWidth = 1.0/double(maxNbChunks);
	double usedWidth = chunkWidth * nChunks;

	QVector<FoldOption*> options;
	for (int i = 0; i <= maxNbChunks - nChunks; i++)
	{
		double startPos = chunkWidth * i;

		// right
		QString fnid1 = QString("%1:%2_%3_L_%4").arg(mID).arg(nSplits).arg(nChunks).arg(startPos);
		options << genFoldOption(fnid1, true, usedWidth, startPos, nSplits);

		// left
		QString fnid2 = QString("%1:%2_%3_R_%4").arg(mID).arg(nSplits).arg(nChunks).arg(startPos);
		options << genFoldOption(fnid2, false, usedWidth, startPos, nSplits);
	}

	return options;
}

FoldOption* ChainScaff::genDeleteFoldOption()
{
	// keep the number of splits for computing the cost
	QString fnid = mID + "_delete";
	int nS = ParSingleton::instance()->maxNbSplits;
	FoldOption* delete_fn = new FoldOption(fnid, true, 0, 0, nS);
	return delete_fn;
}


void ChainScaff::resetChainParts(FoldOption* fn)
{
	// clear
	for (PatchNode* n : chainParts) removeNode(n->mID);
	chainParts.clear();

	// clone original slave
	PatchNode* slave = (PatchNode*)origSlave->clone();
	Structure::Graph::addNode(slave);

	// shrink along joint direction
	int aid_h = slave->mBox.getAxisId(baseJoint.Direction);
	Vector3 boxAxis = slave->mBox.Axis[aid_h];
	double t0 = fn->position;
	double t1 = fn->position + fn->scale;
	if (dot(baseJoint.Direction, boxAxis) > 0)
		slave->mBox.scaleRange01(aid_h, t0, t1);
	else slave->mBox.scaleRange01(aid_h, 1-t1, 1-t0);

	// update mesh and scaffold
	slave->deformMesh();
	slave->createScaffold(true);

	// cut planes from bottom to up
	QVector<Geom::Plane> cutPlanes;
	Geom::Rectangle baseRect = baseMaster->mPatch;
	for (Vector3 cpoint : cutPoints)
	{
		Geom::Rectangle crect = baseRect.translated(cpoint - baseRect.Center);
		cutPlanes << crect.getPlane();

		// debug
		//visDebug.addRectangle(crect, Qt::yellow);
	}

	// split and sort the parts from bottom to the top
	for (ScaffNode* n : Scaffold::split(origSlave->mID, cutPlanes))
	{
		n->properties["dist"] = (n->center() - slaveSeg.P0).norm();
		chainParts << (PatchNode*)n;
	}
	qSort(chainParts.begin(), chainParts.end(),
		[](PatchNode* n1, PatchNode* n2){
		double dist1 = n1->properties["dist"].value<double>();
		double dist2 = n2->properties["dist"].value<double>();
		return dist1 < dist2; 
	});

	// encode the salve top
	slaveTopCoord = chainParts.last()->mBox.getCoordinates(slaveSeg.P1);
}

void ChainScaff::resetHingeLinks()
{
	// remove hinge links
	qDeleteAll(links);	links.clear();
	rightLinks.clear(); leftLinks.clear();

	// basic orientations
	double jointLen = baseJoint.length();
	Vector3 slaveV = slaveSeg.Direction;
	Vector3 jointV = baseJoint.Direction;

	// ***hinge links between base and slave
	Hinge* hingeR = new Hinge(chainParts[0], baseMaster, 
		baseJoint.P0, slaveV, rightDirect, jointV, jointLen);
	Hinge* hingeL = new Hinge(chainParts[0], baseMaster, 
		baseJoint.P1, slaveV, -rightDirect, -jointV, jointLen);
	ScaffLink* linkR = new ScaffLink(chainParts[0], baseMaster, hingeR);
	ScaffLink* linkL = new ScaffLink(chainParts[0], baseMaster, hingeL);
	Graph::addLink(linkR);	
	Graph::addLink(linkL);
	rightLinks << linkR; 
	leftLinks << linkL;

	// ***hinge links between two parts in the chain
	Vector3 jointOffsetR(0, 0, 0);
	if (ParSingleton::instance()->useThk)
		jointOffsetR = slaveHThk * origSlave->mPatch.Normal;
	for (int i = 0; i < chainParts.size() - 1; i++)
	{
		PatchNode* highPart = chainParts[i+1];
		PatchNode* lowPart = chainParts[i];

		Vector3 offsetV = cutPoints[i] - baseJoint.Center;
		Geom::Segment joint = baseJoint.translated(offsetV);
		Hinge* hingeR = new Hinge(highPart, lowPart, 
			joint.P0 + jointOffsetR, slaveV, -slaveV, jointV, jointLen);
		Hinge* hingeL = new Hinge(highPart, lowPart, 
			joint.P1 - jointOffsetR, slaveV, -slaveV, -jointV, jointLen);

		ScaffLink* linkR = new ScaffLink(highPart, lowPart, hingeR);
		ScaffLink* linkL = new ScaffLink(highPart, lowPart, hingeL);
		Graph::addLink(linkR);	
		Graph::addLink(linkL);
		rightLinks << linkR; 
		leftLinks << linkL;
	}
}

void ChainScaff::activateLinks()
{
	// clear
	activeLinks.clear();

	// set hinge for each joint
	for (int i = 0; i < rightLinks.size(); i++)
	{
		// inactive both hinges
		rightLinks[i]->removeTag(ACTIVE_LINK_TAG);
		leftLinks[i]->removeTag(ACTIVE_LINK_TAG);

		// active link
		ScaffLink* activeLink;
		if (i % 2 == 0)
			activeLink = (foldToRight) ? rightLinks[i] : leftLinks[i];
		else
			activeLink = (foldToRight) ? leftLinks[i] : rightLinks[i];

		activeLink->addTag(ACTIVE_LINK_TAG);
		activeLinks << activeLink;
	}
}

void ChainScaff::applyFoldOption( FoldOption* fn)
{
	// delete chain if fold option is null
	if (fn->scale == 0) {
		isDeleted = true;
		//std::cout << "Chain: " << mID.toStdString() << " is deleted.\n";
		return;
	}
	
	// clear tag
	isDeleted = false; 

	// reset chains and hinges
	computeCutPoints(fn);
	resetChainParts(fn);
	resetHingeLinks();

	// active
	foldToRight = fn->rightSide;
	activateLinks();
}

double ChainScaff::getSlaveArea()
{
	return origSlave->mPatch.area();
}

void ChainScaff::showBasicOrientations()
{
	// debug
	visDebug.addSegment(baseJoint, Qt::blue);
	visDebug.addSegment(topJoint, Qt::blue);
	visDebug.addSegment(slaveSeg, Qt::black);
	visDebug.addSegment(upSeg, Qt::green);
	visDebug.addSegment(rightSeg, Qt::red);
	Geom::Segment rightV(baseJoint.P0, baseJoint.P0 + baseJoint.length() * rightDirect);
	visDebug.addSegment(rightV, Qt::red);
}
