#pragma once

#include "Scaffold.h"

class UnitScaff;

// SuperShapeKf is the snapshot of the entire object at certain time
// super patches are used to replace the clasped parts

class SuperShapeKf final : public Scaffold
{
public:
	SuperShapeKf(Scaffold* superKeyframe, StringSetMap moc_g);

	// valid only if all order constraints are met
	bool isValid();

	// parts in between two regular masters
	QVector<ScaffNode*> getPartsInbetween(QString base_mid, QString top_mid);

	// unrelated masters with the super master containing the regular master
	QVector<ScaffNode*> getMastersUnrelatedTo(QString regular_mid);

	// folded parts on the base master
	QVector<ScaffNode*> getFoldedPartsOn(QString base_mid);
	Vector3 getOffsetOfFoldedPartsOn(QString base_mid);

public:
	// the map between master and super master
	QMap<QString, QString> master2SuperMap;

	// the oder constrains using super masters
	StringSetMap mocGreater, mocLess;
};

