#include "FoldManager.h"
#include "FdUtility.h"
#include "ChainScaff.h"
#include "ParSingleton.h"
#include "Configurer.h"

#include <QFileDialog>
#include <QDir>
#include <QElapsedTimer>

FoldManager::FoldManager()
{
	inputScaffold = nullptr;
	confScaffold = nullptr;
	shapeDec = nullptr;

	// react to parameter changes
	this->connect(ParSingleton::instance(), SIGNAL(sqzVChanged()), SLOT(configure()));
	this->connect(ParSingleton::instance(), SIGNAL(inPlaceChanged()), SLOT(configure()));
}

FoldManager::~FoldManager()
{
	if (shapeDec) delete shapeDec;
}

// input
void FoldManager::setInputScaffold( Scaffold* scaff )
{
	inputScaffold = scaff;

	delete confScaffold;
	confScaffold = nullptr;
	delete shapeDec;
	shapeDec = nullptr;

	updateUnitList();
	updateKeyframeSlider();
}

// selection
void FoldManager::selectUnit( QString id )
{
	if (shapeDec)
		shapeDec->selectUnit(id);

	updateChainList();

	emit(contentChanged());
}

void FoldManager::selectChain( QString id )
{ 
	if (shapeDec)
		getSelUnit()->selectChain(id);

	emit(contentChanged());
}

UnitScaff* FoldManager::getSelUnit()
{
	if (shapeDec)
		return shapeDec->getSelUnit();
	else
		return nullptr;
}

void FoldManager::selectKeyframe( int idx )
{
	if (!shapeDec) return;

	shapeDec->selectKeyframe(idx);
	emit(contentChanged());
}

Scaffold* FoldManager::getSelKeyframe()
{
	if (!shapeDec) return nullptr;

	return shapeDec->getSelKeyframe();
}

void FoldManager::updateUnitList()
{
	QStringList unitLabels;
	if (shapeDec)
		unitLabels = shapeDec->getUnitLabels();

	emit(unitsChanged(unitLabels));

	updateChainList();
}

void FoldManager::updateChainList()
{
	QStringList chainLables;
	if (getSelUnit())
		chainLables = getSelUnit()->getChainLabels();

	emit(chainsChanged(chainLables));
}

void FoldManager::updateKeyframeSlider()
{
	if (!shapeDec) return;

	emit(keyframesChanged(shapeDec->keyframes.size()));
}

Scaffold* FoldManager::activeScaffold()
{
	if (shapeDec)
		return shapeDec->activeScaffold();
	else	
		return inputScaffold;
}

// foldabilization
void FoldManager::configure()
{
	if (!inputScaffold) return;

	if (confScaffold) delete confScaffold;
	confScaffold = (Scaffold*)inputScaffold->clone();
	Configurer conf(confScaffold);

	// show configured scaffold
	ParSingleton::instance()->setWhat2Show(ParSingleton::CONF_SCAFF);

	emit(contentChanged());
}

void FoldManager::disconnect()
{
	if (!confScaffold) return;

	// remove link
	QVector<QString> selNids = confScaffold->getSelectedNodeIds();
	if (selNids.size() != 2) return; // two selected nodes
	bool removed = confScaffold->removeLink(selNids.front(), selNids.back());

	if (removed)
	{
		// create (edge rod) virtual master
		// since each slave must link to exactly two masters
		// edge rods perp to sqzV
		QVector<Structure::Node*> selNodes = confScaffold->getSelectedNodes();
		ScaffNode* n1 = (ScaffNode*)selNodes.front();
		ScaffNode* n2 = (ScaffNode*)selNodes.back();
		ScaffNode* slave = n1->isMaster ? n2 : n1;
		PatchNode* master = (PatchNode*)(n1->isMaster ? n1 : n2);
		if (slave->mType == ScaffNode::ROD)
		{
			// todo: create virtual base 
			confScaffold->removeNode(slave->mID);
		}
		else
		{
			// find the closest rod node
			PatchNode* slavePatch = (PatchNode*)slave;
			RodNode* virtualRn = nullptr;
			double minDist = maxDouble();
			Vector3 sqzV = ParSingleton::instance()->sqzV;
			for (RodNode* rn : slavePatch->getEdgeRodNodes())
			{
				// perp to squeezing direction
				if (rn->isPerpTo(sqzV, 0.1))
				{
					double dist = (rn->center() - master->center()).norm();

					// keep track of the furthest
					if (dist < minDist)
					{
						minDist = dist;
						if (virtualRn) delete virtualRn;
						virtualRn = (RodNode*)rn->clone();
					}
				}

				// garbage collection
				delete rn;
			}

			// create virtual base patch
			PatchNode* virtualPn = new PatchNode(virtualRn, sqzV);
			virtualPn->addTag(EDGE_VIRTUAL_TAG);
			virtualPn->isMaster = true;
			confScaffold->Structure::Graph::addNode(virtualPn);

			// create link
			ScaffLink* link = new ScaffLink(slave, virtualPn);
			confScaffold->Structure::Graph::addLink(link);
		}

	}

	// update content drawing
	emit(contentChanged());
}


void FoldManager::decompose()
{
	// configure the input scaffold
	if (!confScaffold) configure();

	// create decomposition
	if (shapeDec) delete shapeDec;
	shapeDec = new DecScaff("", confScaffold);

	// update unit list and key frame slider
	updateUnitList();
	updateKeyframeSlider();

	// show decomposition
	ParSingleton::instance()->setWhat2Show(ParSingleton::DECOMP);
}

void FoldManager::generateKeyframes()
{
	// generate
	if (shapeDec)
	{
		shapeDec->genKeyframes();
	}

	// update slider on UI
	updateKeyframeSlider();
}

void FoldManager::foldabilize()
{
	// assert input
	if (!inputScaffold)	return;

	// start timer
	QElapsedTimer timer;
	timer.start();

	// foldabilize
	message("Decomposing...");
	decompose();
	message("Foldabilizing...");
	shapeDec->foldabilize(); 
	
	// end timer
	elapsedTime = timer.elapsed();
	message(QString("Foldabilizing...Done : %1s").arg(elapsedTime / 1000));

	// generate key frames
	message("Generating keyframes...");
	shapeDec->genKeyframes();
	updateKeyframeSlider();
	message("Generating keyframes...Done!");

	// show key frames
	shapeDec->selectKeyframe(0);
	ParSingleton::instance()->setWhat2Show(ParSingleton::KEYFRAME);

	// statistics
	//exportStat();
}

void FoldManager::exportResultMesh()
{
	//if (results.isEmpty()) return;

	//// Get results output folder
	//QString dataPath = QFileDialog::getExistingDirectory(0, tr("Export Results"), getcwd());
	//// Create folder
	//dataPath += "//" + activeScaffold()->mID;
 //   QDir d(""); d.mkpath(dataPath);

	//for(int i = 0; i < results.size(); i++){
	//	// Create folder
	//	QString currPath = dataPath + "//result"+ QString::number(i+1);
	//	d.mkpath(currPath);
	//	for(int j = 0; j < results[i].size(); j++){
	//		QString filename = currPath + "//" + QString::number(j) + ".obj";
	//		results[i][j]->exportMesh(filename);
	//	}
	//}
}

//void FoldManager::exportStat()
//{
//	QString filename = QFileDialog::getSaveFileName(0, tr("Save Statistics"), nullptr, tr("Txt file (*.txt)"));
//
//	QFile file(filename);
//	if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) return;
//	QTextStream out(&file);
//
//	 fold parameters
//	ParSingleton* ps = ParSingleton::instance();
//	out << QString("%1 = %2, %3, %4\n").arg("sqzDirct").arg(ps->sqzV[0]).arg(ps->sqzV[1]).arg(ps->sqzV[2]);
//	out << QString("%1 = %2, %3, %4\n").arg("aabbCstrScale").arg(ps->aabbCstrScale[0]).arg(ps->aabbCstrScale[1]).arg(ps->aabbCstrScale[2]);
//	out << QString("%1 = %2\n\n").arg("connThrRatio").arg(ps->connThrRatio);
//
//	out << QString("%1 = %2\n").arg("maxNbSplits").arg(ps->maxNbSplits);
//	out << QString("%1 = %2\n\n").arg("maxNbChunks").arg(ps->maxNbChunks);
//	out << QString("%1 = %2\n").arg("splitWeight").arg(ps->splitWeight);
//
//	 timing
//	out << QString("%1 = %2\n").arg("elapsedTime").arg(elapsedTime);
//
//
//	 space saving
//	Scaffold* lastKeyframe = shapeDec->keyframes.last();
//	double origVol = inputScaffold->computeAABB().box().volume();
//	double fdVol = lastKeyframe->computeAABB().box().volume();
//	double spaceSaving = 1 - fdVol / origVol;
//	out << QString("%1 = %2\n\n").arg("spaceSaving").arg(spaceSaving);
//
//	 resulted scaffold
//	out << QString("%1 = %2\n").arg("nbMasters").arg(shapeDec->masters.size());
//	out << QString("%1 = %2\n").arg("nbSlaves").arg(shapeDec->slaves.size());
//	out << QString("%1 = %2\n\n").arg("nbUnits").arg(shapeDec->units.size());
//
//	int nbHinges = 0;
//	double shrinkedArea = 0, totalArea = 0;
//	for (UnitScaff* unit : shapeDec->units){
//		for (ChainScaff* chain : unit->chains)
//		{
//			nbHinges += chain->nbHinges;
//			shinkedArea += chain->shrinkedArea;
//			totalArea += chain->patchArea;
//		}
//	}
//	for (PatchNode* m : shapeDec->masters)
//		totalArea += m->mPatch.area();
//	out << QString("%1 = %2\n").arg("nbHinges").arg(nbHinges);
//	out << QString("%1 = %2\n\n").arg("shrinkedArea").arg(shrinkedArea);
//}