#include "ScaffManager.h"
#include "SegMeshLoader.h"
#include "RodNode.h"
#include "PatchNode.h"
#include "Scaffold.h"
#include "ParSingleton.h"

#include <QFileInfo>
#include <QFileDialog>


ScaffManager::ScaffManager()
{
	wholeMesh = nullptr;
	scaffold = nullptr;

	fitMethod = FIT_AABB;
	refitMethod = FIT_MIN;
}


ScaffManager::~ScaffManager()
{
	delete scaffold;
}


void ScaffManager::createScaffold()
{
	// mesh segments
	if (!wholeMesh) return;
	SegMeshLoader sml(wholeMesh);
	QVector<SurfaceMeshModel*> subMeshes = sml.getSegMeshes();

	// reset scaffold
	if (scaffold) delete scaffold;
	scaffold = new Scaffold;
	scaffold->path = wholeMesh->path;

	// fit nodes from meshes
	for (SurfaceMeshModel* m : subMeshes)
		scaffold->addNode(MeshPtr(m), fitMethod);

	emit(scaffoldChanged(scaffold));
}

void ScaffManager::saveScaffold()
{
	if (!scaffold) return;

	QString dir = QFileInfo(scaffold->path).absolutePath();
	QFileInfo fileInfo(scaffold->path);
	QString filename = QFileDialog::getSaveFileName(0, tr("Save Scaffold"), dir, tr("Graph Files (*.xml)"));
	scaffold->saveToFile(filename);

	emit(message("Saved."));
}

void ScaffManager::loadScaffold()
{
	// file name
	QString dataPath = getcwd() + "/data-syn";
	QString filename = QFileDialog::getOpenFileName(0, tr("Load Scaffold"), dataPath, tr("Graph Files (*.xml)"));
	if (filename.isEmpty()) return;

	// load
	if (scaffold) delete scaffold;
	scaffold = new Scaffold;
	scaffold->path = filename;
	scaffold->loadFromFile(filename);

	// show options
	ParSingleton* ps = ParSingleton::instance();
	ps->restoreDefault();
	ps->setWhat2Show(ParSingleton::INITIAL_SCAFF);
	emit(scaffoldChanged(scaffold));
	emit(message("Scaffold loaded."));
}

void ScaffManager::setMesh( Model* mesh )
{
	this->wholeMesh = (SurfaceMeshModel*)mesh;
	qDebug() << "Set active mesh as " << wholeMesh->path;
}

void ScaffManager::changeNodeType()
{
	for (Structure::Node* n : scaffold->getSelectedNodes())
	{
		scaffold->changeNodeType((ScaffNode*)n);
	}

	emit(contentChanged());
}

void ScaffManager::refitNodes()
{
	for (Structure::Node* n : scaffold->getSelectedNodes())
	{
		ScaffNode* fn = (ScaffNode*)n;
		fn->refit(refitMethod); 
	}

	emit(contentChanged());
}

void ScaffManager::setFitMethod( int method )
{
	if (method == 0)
		fitMethod = FIT_AABB;
	else if (method == 1)
		fitMethod = FIT_MIN;
	else
		fitMethod = FIT_PCA;
}

void ScaffManager::setRefitMethod( int method )
{
	if (method == 0)
		fitMethod = FIT_MIN;
	else if (method == 1)
		fitMethod = FIT_AABB;
	else
		fitMethod = FIT_PCA;
}