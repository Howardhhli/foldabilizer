#include "Configurer.h"
#include "ParSingleton.h"

Configurer::Configurer(Scaffold* scaff)
{
	scaffold = scaff;

	// configure
	createMasters();
	createSlaves();
	createLinks();

	// remove unbounded slaves
	for (ScaffNode* sn : slaves)
	{
		if (scaffold->nbLinks(sn->mID) != 2)
			scaffold->removeNode(sn->mID);
	}
}

Configurer::~Configurer()
{

}


double Configurer::getConnectThr()
{
	double connThrRatio = ParSingleton::instance()->connThrRatio;
	return connThrRatio * scaffold->computeAABB().radius();
}

ScaffNodeArray2D Configurer::getPerpConnGroups()
{
	// squeezing direction
	Vector3 sqzV = ParSingleton::instance()->sqzV;
	Geom::AABB shapeAabb = scaffold->computeAABB();
	Geom::Box box = shapeAabb.box();
	int sqzAId = shapeAabb.box().getAxisId(sqzV);

	// threshold
	double perpThr = 0.1;
	double connThr = getConnectThr();

	// ==STEP 1==: nodes perp to squeezing direction
	QVector<PatchNode*> perpNodes;
	for (ScaffNode* n : scaffold->getScaffNodes())
	{
		// perp node: rod or patch
		if (n->isPerpTo(sqzV, perpThr))
		{
			PatchNode* pn;
			if (n->mType == ScaffNode::ROD)
				pn = scaffold->changeRodToPatch((RodNode*)n, sqzV);
			else
				pn = (PatchNode*)n;

			perpNodes << pn;
		}
		else if (n->mType == ScaffNode::PATCH)
		{
			// edge rods perp to sqzV
			PatchNode* pn = (PatchNode*)n;
			for (RodNode* rn : pn->getEdgeRodNodes())
			{
				// add virtual rod nodes 
				if (rn->isPerpTo(sqzV, perpThr))
				{
					PatchNode* prn = new PatchNode(rn, sqzV);
					prn->addTag(EDGE_VIRTUAL_TAG);
					scaffold->Structure::Graph::addNode(prn);

					perpNodes << prn;
				}

				// garbage collection
				delete rn;
			}
		}
	}

	// ==STEP 2==: group perp nodes
	// perp positions
	Geom::Box shapeBox = shapeAabb.box();
	Geom::Segment skeleton = shapeBox.getSkeleton(sqzAId);
	double perpGroupThr = connThr / skeleton.length();
	QMultiMap<double, ScaffNode*> posNodeMap;
	for (ScaffNode* n : perpNodes){
		posNodeMap.insert(skeleton.getProjCoordinates(n->mBox.Center), n);
	}
	ScaffNodeArray2D perpGroups;
	QList<double> perpPos = posNodeMap.uniqueKeys();
	double pos0 = perpPos.front();
	perpGroups << posNodeMap.values(pos0).toVector();
	for (int i = 1; i < perpPos.size(); i++)
	{
		QVector<ScaffNode*> currNodes = posNodeMap.values(perpPos[i]).toVector();
		if (fabs(perpPos[i] - perpPos[i - 1]) < perpGroupThr)
			perpGroups.last() += currNodes;	// append to current group
		else perpGroups << currNodes;		// create new group
	}

	// ==STEP 3==: perp & connected groups
	ScaffNodeArray2D perpConnGroups;
	perpConnGroups << perpGroups.front(); // ground
	for (int i = 1; i < perpGroups.size(); i++){
		for (QVector<ScaffNode*> connGroup : getConnectedGroups(perpGroups[i], connThr))
			perpConnGroups << connGroup;
	}

	return perpConnGroups;
}

void Configurer::createMasters()
{
	ScaffNodeArray2D perpConnGroups = getPerpConnGroups();
	Vector3 sqzV = ParSingleton::instance()->sqzV;

	// merge connected groups into patches
	masters.clear();
	for (auto pcGroup : perpConnGroups)
	{
		// single node
		if (pcGroup.size() == 1)
		{
			masters << (PatchNode*)pcGroup.front();
		}
		// multiple nodes
		else
		{
			// check if all nodes in the pcGroup is virtual
			bool allVirtual = true;
			for (ScaffNode* pcNode : pcGroup){
				if (!pcNode->hasTag(EDGE_VIRTUAL_TAG))
				{
					allVirtual = false;
					break;
				}
			}

			// create bundle master
			if (!allVirtual)
				masters << (PatchNode*)scaffold->wrapAsBundleNode(getIds(pcGroup), sqzV);
			// treat edge virtual nodes individually
			else for (ScaffNode* pcNode : pcGroup)
				masters << (PatchNode*)pcNode;
		}
	}

	// normal and tag
	for (PatchNode* m : masters)
	{
		// consistent normal with sqzV
		if (dot(m->mPatch.Normal, sqzV) < 0)
			m->mPatch.flipNormal();

		// tag
		m->isMaster = true;
	}
}

void Configurer::createSlaves()
{
	// split slave parts by master patches
	double connThr = getConnectThr();
	for (PatchNode* master : masters)
	{
		// skip single edge virtual
		if (master->hasTag(EDGE_VIRTUAL_TAG)) continue;

		// split
		for (ScaffNode* n : scaffold->getScaffNodes())
		{
			// skip masters
			if (n->isMaster) continue;

			// split connected slaves
			if (hasIntersection(n, master, connThr))
				scaffold->split(n->mID, master->mPatch.getPlane());
		}
	}

	// non-master parts as slaves
	slaves.clear();
	for (ScaffNode* n : scaffold->getScaffNodes())
	{
		if (!n->isMaster)
		{
			slaves << n;
		}
	}
}

void Configurer::createLinks()
{
	// find two masters attaching to a slave
	double adjacentThr = getConnectThr();
	for (int i = 0; i < slaves.size(); i++)
	{
		// find adjacent master(s)
		ScaffNode* slave = slaves[i];
		for (int j = 0; j < masters.size(); j++)
		{
			PatchNode* master = masters[j];

			// skip if not attached
			if (getDistance(slave, master) > adjacentThr) continue;


			// virtual edge rod master
			if (master->hasTag(EDGE_VIRTUAL_TAG))
			{
				// can only attach to its host slave
				QString masterOrig = master->properties[EDGE_ROD_ORIG].value<QString>();
				QString slaveOrig = slave->mID;
				if (slave->properties.contains(SPLIT_ORIG))
					slaveOrig = slave->properties[SPLIT_ORIG].value<QString>();
				if (masterOrig == slaveOrig)
				{
					ScaffLink* link = new ScaffLink(slaves[i], masters[j]);
					scaffold->Structure::Graph::addLink(link);
				}
			}
			// real master
			else
			{
				ScaffLink* link = new ScaffLink(slaves[i], masters[j]);
				scaffold->Structure::Graph::addLink(link);
			}
		}
	}
}
