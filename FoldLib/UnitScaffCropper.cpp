#include "UnitScaffCropper.h"
#include "ChainScaff.h"
#include "BundlePatchNode.h"

UnitScaffCropper::UnitScaffCropper(UnitScaff* unit, Vector3 baseOffset)
{
	// masters, slaves and relations
	for (PatchNode* m : unit->masters)
		masters << (PatchNode*)m->clone();
	for (ChainScaff* chain : unit->chains)
	{
		slaves << (ScaffNode*)chain->origSlave->clone();

		QVector<QString> mp;
		mp << chain->topMaster->mID;
		mp << chain->baseMaster->mID;
		masterPairs << mp;
	}

	// the cut plane
	PatchNode* baseMasterOld = unit->baseMaster;
	Geom::Plane cutPlane = baseMasterOld->getSurfacePlane(true);
	cutPlane.translate(baseOffset);
	
	// cut slaves: collect both parts
	QVector<ScaffNode*> baseSubNodes;
	for (ScaffNode*& slave : slaves)
	{
		QVector<ScaffNode*> subNodes = slave->split(cutPlane, 0.01);
		if (subNodes.isEmpty()) continue;

		// the new slave
		delete slave; slave = subNodes.last();
		
		// the root
		baseSubNodes << subNodes.front();
	}

	// update the base master
	if (!baseSubNodes.isEmpty())
	{
		// create bundle patch
		baseSubNodes << baseMasterOld;
		QString bid = baseMasterOld->mID;
		Geom::Box box = getBundleBox(baseSubNodes);
		BundlePatchNode* baseMasterNew = 
			new BundlePatchNode(bid, box, baseSubNodes, baseMasterOld->mPatch.Normal);
		
		// normal
		if (dot(baseMasterOld->mPatch.Normal, baseMasterNew->mPatch.Normal) < 0)
			baseMasterNew->mPatch.flipNormal();

		// replace
		for (PatchNode*& m : masters)
		{
			if (m->mID == baseMasterNew->mID)
			{
				delete m;
				m = baseMasterNew;
				break;
			}
		}
	}
}
