#include "DecScaff.h"
#include "PatchNode.h"
#include <QFileInfo>
#include "FdUtility.h"
#include "ChainScaff.h"
#include "TUnitScaff.h"
#include "HUnitScaff.h"
#include "ZUnitScaff.h"
#include "Decomposer.h"
#include "ParSingleton.h"

DecScaff::DecScaff(QString id, Scaffold* scaffold)
	: Scaffold(*scaffold)
{
	path = QFileInfo(path).absolutePath();
	mID = id;

	// decompose into units
	decomp = new Decomposer(this);

	// calculate aabb constraint
	ParSingleton* ps = ParSingleton::instance();
	Geom::Box aabb = computeAABB().box();
	aabb.scale(ps->aabbCstrScale);
	ps->aabbCstr = aabb;

	// time scale
	double totalDuration = 0;
	for (UnitScaff* b : units) totalDuration += b->getNbTopMasters();
	timeScale = 1.0 / totalDuration;

	// selection
	selUnitIdx = -1;
	keyframeIdx = -1;
}

DecScaff::~DecScaff()
{
	for (UnitScaff* l : units)	delete l;

	delete decomp;
}

UnitScaff* DecScaff::getSelUnit()
{
	if (selUnitIdx >= 0 && selUnitIdx < units.size())
		return units[selUnitIdx];
	else
		return nullptr;
}

Scaffold* DecScaff::activeScaffold()
{
	UnitScaff* selUnit = getSelUnit();
	if (selUnit) 
		return selUnit->activeScaffold();
	else		  
		return nullptr;
}

QStringList DecScaff::getUnitLabels()
{
	QStringList labels;
	for (UnitScaff* l : units)
		labels.append(l->mID);

	// append string to select none
	labels << "--none--";

	return labels;
}

void DecScaff::selectUnit( QString id )
{
	// select layer named id
	selUnitIdx = -1;
	for (int i = 0; i < units.size(); i++){
		if (units[i]->mID == id){
			selUnitIdx = i;
			break;
		}
	}

	// disable selection on chains
	if (getSelUnit())
		getSelUnit()->selectChain("");
}


void DecScaff::storeDebugInfo(Scaffold* kf, int uidx)
{
	// assert
	if (!kf || uidx < 0 || uidx >= units.size()) return;

	// the active unit
	UnitScaff* unit = units[uidx];

	// highlight active parts
	for (QString nid : unit->getSlnSlaveParts())
	{
		ScaffNode* node = (ScaffNode*)kf->getNode(nid);
		if (node) node->beingFolded = true;
	}

	return;

	// aabb constraint
	kf->visDebug.addBox(ParSingleton::instance()->aabbCstr, Qt::cyan);

	// obstacles
	kf->visDebug.addPoints(unit->getCurrObstacles(), Qt::blue);

	// solution
	kf->visDebug.addRectangles(unit->getCurrSlnFRs(), Qt::green);
}

Scaffold* DecScaff::genKeyframe( double t )
{
	// keyframe of each unit
	QVector<Scaffold*> uKeyframes;
	int activeUnitIdx = -1;
	for (int i = 0; i < units.size(); i++)
	{
		double lt = units[i]->mFoldDuration.getLocalTime(t);
		uKeyframes << units[i]->getKeyframeHub(lt);

		// active unit index
		if (lt > 0 && lt < 1) activeUnitIdx = i;
	}

	// combine
	Scaffold *keyframe = new Scaffold(uKeyframes, decomp->baseMaster->mID, decomp->masterUnitMap);
	
	// garbage collection
	for (Scaffold* unit : uKeyframes) delete unit;

	// debug
	storeDebugInfo(keyframe, activeUnitIdx);

	return keyframe;
}

void DecScaff::genKeyframes()
{
	keyframes.clear();

	int N = ParSingleton::instance()->nbKeyframes;
	double step = 1.0 / (N-1);
	for (int i = 0; i < N; i++)
	{
		Scaffold* kf = genKeyframe(i * step);
		if(!kf) return;

		// unwrap bundles to view real parts
		kf->unwrapBundleNodes();
		kf->removeNodesWithTag(EDGE_VIRTUAL_TAG);

		// save the key frame
		keyframes << kf;
	}
}

SuperShapeKf* DecScaff::genSuperShapeKf( double t )
{
	// super key frame for each block
	QVector<Scaffold*> uSuperKf;
	for (UnitScaff* unit : units)
	{
		Scaffold* uk_super;
		if (unit->mFoldDuration.start >= 1)
		{// the unit has not been foldabilized		
			uk_super = (Scaffold*)unit->clone();
		}else{
			double lt = unit->mFoldDuration.getLocalTime(t);
			uk_super = unit->genSuperKeyframe(lt);
		}

		uSuperKf << uk_super;
	}

	// combine using regular masters shared between units
	// all super masters and slaves will be cloned into the superKf
	Scaffold *superKf = new Scaffold(uSuperKf, decomp->baseMaster->mID, decomp->masterUnitMap);

	// create super shape key frame
	// super masters sharing common regular masters will be grouped
	SuperShapeKf* superShapeKf = new SuperShapeKf(superKf, decomp->masterOrderGreater);

	// garbage collection
	delete superKf;
	for (int i = 0; i < uSuperKf.size(); i++) delete uSuperKf[i];

	// return
	return superShapeKf;
}


void DecScaff::foldabilize()
{
	// initialization
	for (UnitScaff* u : units){
		u->initFoldSolution();
		u->mFoldDuration.set(1.0, 2.0); // t > 1.0 means not be folded
	}

	// start optimization
	std::cout << "\n============START============\n";
	double currTime = 0.0;
	SuperShapeKf* currKeyframe = genSuperShapeKf(currTime);

	// choose best unfolded unit
	UnitScaff* next_unit = getBestNextUnit(currTime, currKeyframe);
	while (next_unit)
	{
		std::cout << "**\nBest next = " << next_unit->mID.toStdString() << "\n"
				  << "Foldabilizing...\n";

		// foldabilize
		double timeLength = next_unit->getNbTopMasters() * timeScale;
		double nextTime = currTime + timeLength;
		next_unit->foldabilizeHub(currKeyframe, TimeInterval(currTime, nextTime));
		std::cout << "\n-----------//-----------\n";

		// get best next
		currTime = nextTime;
		delete currKeyframe;
		currKeyframe = genSuperShapeKf(currTime);
		next_unit = getBestNextUnit(currTime, currKeyframe);
	}

	// interlocking units
	if (decomp->mergeInterlockUnits())
	{
		UnitScaff* mergedUnit = units.last();
		mergedUnit->initFoldSolution();
		mergedUnit->foldabilizeHub(currKeyframe, TimeInterval(currTime, 1.0));
	}

	// finish
	delete currKeyframe;
	std::cout << "\n============FINISH============\n";
}

double DecScaff::foldabilizeUnit(UnitScaff* unit, double currTime, SuperShapeKf* currKf,
										double& nextTime, SuperShapeKf*& nextKf)
{
	// foldabilize nextUnit wrt the current super key frame
	double timeLength = unit->getNbTopMasters() * timeScale;
	nextTime = currTime + timeLength;
	double cost = unit->foldabilizeHub(currKf, TimeInterval(currTime, nextTime));

	// get the next super key frame 
	nextKf = genSuperShapeKf(nextTime);

	// the normalized cost wrt. the importance
	cost *= unit->importance;
	return cost;
}

double DecScaff::lookahead(double currT, SuperShapeKf* currKeyframe, QVector<UnitScaff*> ufUnits)
{
	// one-step lookahead
	std::cout << "Looking ahead...\n";

	// total cost of foldabilizing unfolded units w.r.t. currKeyframe
	double totalCost = 0;
	for (UnitScaff* nextUnit : ufUnits)
	{
		// skip the unit who starts the lookahead
		if (nextUnit->mFoldDuration.end == currT) continue;

		// back up time interval
		TimeInterval origNextTi = nextUnit->mFoldDuration;

		// foldabilize nextUnit
		std::cout << "==> " << nextUnit->mID.toStdString() << "\n";
		double nextTime;
		SuperShapeKf* nextKeyframe;
		double cost = foldabilizeUnit(nextUnit, currT, currKeyframe, nextTime, nextKeyframe);

		// invalid block folding generates large cost as being deleted
		if (!nextKeyframe->isValid()) cost = nextUnit->importance; // deleting cost = 1.0

		delete nextKeyframe; // garbage collection
		nextUnit->mFoldDuration = origNextTi; // restore time interval

		// accumulate total cost
		totalCost += cost;
	}

	return totalCost;
}

UnitScaff* DecScaff::getBestNextUnit(double currTime, SuperShapeKf* currKeyframe)
{
	// unfolded units
	QVector<UnitScaff*> unfoldedUnits;
	for (UnitScaff* u : units){
		if (!u->mFoldDuration.hasPassed(currTime))
			unfoldedUnits << u;
	}

	// trivial cases
	if (unfoldedUnits.isEmpty()) return nullptr;
	if (unfoldedUnits.size() == 1) return unfoldedUnits.front();

	// choose the best
	UnitScaff* nextBest = nullptr;
	double min_cost = maxDouble();
	for (UnitScaff* nextUnit : unfoldedUnits)
	{
		// back up time interval
		TimeInterval origNextTi = nextUnit->mFoldDuration; 

		// foldabilize nextUnit
		std::cout << "*\nEvaluating unit: " << nextUnit->mID.toStdString() << "\n";
		double nextTime = -1.0;
		SuperShapeKf* nextKeyframe = nullptr;
		double nextCost = foldabilizeUnit(nextUnit, currTime, currKeyframe, nextTime, nextKeyframe);

		// the folding of nextUnit must be valid, otherwise skip further evaluation
		if (nextKeyframe->isValid()) 
		{
			// lookahead
			if (ParSingleton::instance()->lookahead)
				nextCost += lookahead(nextTime, nextKeyframe, unfoldedUnits);

			// keep track of the best
			std::cout << "TotalCost = " << nextCost << std::endl;
			if (nextCost < min_cost - ZERO_TOLERANCE_LOW){
				min_cost = nextCost; nextBest = nextUnit;
			}
		}else
			std::cout << "Invalid folding.\n";

		delete nextKeyframe; // garbage collection
		nextUnit->mFoldDuration = origNextTi;// restore time interval
	}

	// found the best next
	return nextBest;
}

Scaffold* DecScaff::getSelKeyframe()
{
	if (keyframeIdx >= 0 && keyframeIdx < keyframes.size())
		return keyframes[keyframeIdx];
	else return nullptr;
}

void DecScaff::selectKeyframe( int idx )
{
	if (idx >= 0 && idx < keyframes.size())
		keyframeIdx = idx;
}