#pragma once

#include "ChainScaff.h"

class HChainScaff final : public ChainScaff
{
public:
	HChainScaff(ScaffNode* slave, PatchNode* base, PatchNode* top);

	// fold option
	virtual Geom::Rectangle getFoldRegion(FoldOption* fn) override;

	// fold options
	virtual QVector<FoldOption*> genRegularFoldOptions() override;

	// cut points
	virtual void computeCutPoints(FoldOption* fn) override;

	// fold
	virtual void fold(double t) override;
	void foldUniformHeight(double t);
	void foldUniformAngle(double t);
	void translateTopMaster();
	void computePhaseSeparator();

public:
	// phase separator
	double				heightSep;	// the height separates phase I and II
	double				angleSep;	// angle between b and base
};


