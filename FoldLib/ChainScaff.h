#pragma once

#include "Scaffold.h"
#include "PatchNode.h"
#include "FoldOptGraph.h"

class ChainScaff : public Scaffold
{
public:
    ChainScaff(ScaffNode* slave, PatchNode* base, PatchNode* top);
	~ChainScaff();
	
	// set up
	void setupMasters(PatchNode* base, PatchNode* top);
	void setupSlave(ScaffNode* slave);
	void setupThickness();
	void computeBasicOrientations();
	void computeRightDirection();

	// debug
	void showBasicOrientations();

	// time interval
	void setFoldDuration(double t0, double t1);

	// the area of original slave patch
	double getSlaveArea();

public:
	// fold options
	FoldOption* genFoldOption(QString id, bool isRight, double width, double startPos, int nSplits); // regular option
	FoldOption* genDeleteFoldOption();	// deleting option
	QVector<FoldOption*> genRegularFoldOptions(int nS, int nC);	// regular options with nS and nC
	virtual QVector<FoldOption*> genRegularFoldOptions() = 0;	// all regular options

	// fold region
	virtual Geom::Rectangle getFoldRegion(FoldOption* fn) = 0;

	// apply fold option: modify the chain
	void applyFoldOption(FoldOption* fn);
	virtual void computeCutPoints(FoldOption* fn) = 0;
	void resetChainParts(FoldOption* fn);
	void resetHingeLinks();
	void activateLinks();

	// folding
	virtual void fold(double t) = 0;

	// key frame
	Scaffold* getKeyframe(double t);

public:
	// parts
	PatchNode*			topMaster;	// top
	PatchNode*			baseMaster;	// base							
	PatchNode*			origSlave;	// original slave
	QVector<PatchNode*>	chainParts;	// sorted parts in the chain, from base to top

	/*
		topJoint				topCenter
			|\						^
			: \						|
			:  \ slaveSeg			| upSeg
			:   \					|
			:    \					|
			:-----> (x)baseJoint	|
	   rightSeg/rightDirect		baseRect
	*/
	// basic orientations
	Geom::Segment	baseJoint;	// joint between slave and base
	Geom::Segment	topJoint;	// joint between slave and top
	Geom::Segment	upSeg;		// up right segment from topCenter's projection to topCenter
	Geom::Segment	slaveSeg;	// 2D abstraction, perp to joints (base to top)
	Geom::Segment	rightSeg;	// right direction, perp to joints
	Vector3			rightDirect;// direction of rightSeg; or the cross product of baseJoint and slaveSeg

	// thickness
	double topHThk, baseHThk, slaveHThk;	// half thickness

	// cut points and hinges
	QVector<Vector3> cutPoints;	// cut points on the slave segment
	QVector<ScaffLink*>	rightLinks, leftLinks, activeLinks;

	// fold parameters
	TimeInterval	duration;	// time interval
	bool			foldToRight;// folding side
	bool			isDeleted;	// deleted fold option has been applied to this chain
	double			importance;	// normalized importance wrt. patch area

	// the top of slave
	Vector3 slaveTopCoord;	// the coordinates of slave top in the top most part
	Vector3 topCenterOrig;	// the original center of top master
};

/*
Thickness:
(1) SetupSlave(): 
	deform the slave to attach to masters perfectly w.r.t. the thickness of masters
(2) computeBasicOrientations(): 
	w.r.t. the round joints between the slave and master (the thickness of the slave).
	slaveSeg and upSeg are cropped, and baseJoint and topJoint are placed at right locations.
(3) resetHingeLinks(): 
	hinges are moved sideway w.r.t. the thickness of the slave
(4) fold():
	the position of top master is determined by the translation of the topJoint
*/