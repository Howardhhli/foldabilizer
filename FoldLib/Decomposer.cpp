#include "Decomposer.h"

#include "DecScaff.h"
#include "TUnitScaff.h"
#include "HUnitScaff.h"
#include "ZUnitScaff.h"
#include "ParSingleton.h"

#include "IntrRect2Rect2.h"

Decomposer::Decomposer(DecScaff* ss)
{
	ds = ss;

	// decompose
	init();
	clusterSlaves();
	createUnits();
	computeUnitImportance();

	// master order constraints
	// to-do: order constraints among all parts
	computeMasterOrderConstraints();
}


void Decomposer::init()
{
	// masters and slaves
	for (ScaffNode* n : ds->getScaffNodes()){
		if (n->isMaster){
			n->properties["master_idx"] = masters.size();
			masters << (PatchNode*)n;
		}
		else{
			n->properties["slave_idx"] = slaves.size();
			slaves << n;
		}
	}

	// master and slave relationship
	slave2master.resize(slaves.size());
	for (Structure::Link* l : ds->links)
	{
		ScaffNode* n1 = (ScaffNode*)l->node1;
		ScaffNode* n2 = (ScaffNode*)l->node2;
		int sid, mid;
		if (n1->isMaster){
			mid = n1->properties["master_idx"].value<int>();
			sid = n2->properties["slave_idx"].value<int>();
		}
		else{
			mid = n2->properties["master_idx"].value<int>();
			sid = n1->properties["slave_idx"].value<int>();
		}
		slave2master[sid] << mid;
	}

	// each slave must link to exactly two masters
	bool allTwo = true;
	for (QSet<int> ms : slave2master)
	{
		if (ms.size() != 2)
		{
			allTwo = false;
			break;
		}
	}
	if (allTwo)
		std::cout << "All slaves are bounded by two masters. \n";
	else
		std::cout << "Exception: slave is not bounded by two masters. \n";
}


void Decomposer::clusterSlaves()
{
	// clear
	slaveClusters.clear();
	QVector<bool> slaveVisited(slaves.size(), false);

	// build master(V)-slave(E) graph
	Structure::Graph* ms_graph = new Structure::Graph();
	for (int i = 0; i <masters.size(); i++)
	{
		Structure::Node* n = new Structure::Node(QString::number(i));
		ms_graph->addNode(n); // masters are nodes
	}
	for (int i = 0; i < slaves.size(); i++)
	{
		// slaves are links
		QList<int> midPair = slave2master[i].toList();
		if (midPair.size() != 2) continue; // exception
		QString nj = QString::number(midPair.front());
		QString nk = QString::number(midPair.last());
		if (ms_graph->getLink(nj, nk) == nullptr)
			ms_graph->addLink(nj, nk);
	}

	// enumerate all chordless cycles
	QVector<QVector<QString> > cycle_base = ms_graph->findCycleBase();
	delete ms_graph;

	// collect slaves along each cycle
	QVector<QSet<int> > cycle_slaves;
	for (QVector<QString> cycle : cycle_base)
	{
		QSet<int> cs;
		for (int i = 0; i < cycle.size(); i++)
		{
			// two master ids
			QSet<int> midPair;
			midPair << cycle[i].toInt() << cycle[(i + 1) % cycle.size()].toInt();

			// find all siblings: slaves sharing the same pair of masters
			for (int sid = 0; sid < slaves.size(); sid++){
				if (midPair == slave2master[sid])
					cs << sid;
			}
		}
		cycle_slaves << cs;
	}

	// merge cycles who share slaves
	mergeIsctSets(cycle_slaves, slaveClusters);
	for (auto cs : slaveClusters)
	for(int sid : cs) slaveVisited[sid] = true;

	// edges that are not covered by cycles form individual clusters
	for (int i = 0; i < slaves.size(); i++)
	{
		// skip covered slaves
		if (slaveVisited[i]) continue;

		// create new cluster
		QSet<int> cluster;
		cluster << i;
		slaveVisited[i] = true;

		// find siblings
		for (int sid = 0; sid < slaves.size(); sid++){
			if (!slaveVisited[sid] && slave2master[i] == slave2master[sid])
			{
				cluster << sid;
				slaveVisited[sid] = true;
			}
		}
		slaveClusters << cluster;
	}
}

void Decomposer::createUnits()
{
	// clear units
	for (auto u : ds->units) delete u;
	ds->units.clear();
	masterUnitMap.clear();

	// each slave cluster forms a block
	for (auto sc : slaveClusters)
		ds->units << createUnit(sc);
}

UnitScaff* Decomposer::createUnit(QSet<int> sCluster)
{
	// slaves and master pairs
	QVector<ScaffNode*> ss;
	QVector< QVector<QString> > mPairs;
	QSet<int> mids; // unique master indices
	for (int sidx : sCluster)
	{
		ss << slaves[sidx]; // slaves

		QVector<QString> mp;
		for (int mid : slave2master[sidx]){
			mids << mid;
			mp << masters[mid]->mID;
		}

		mPairs << mp; // master pairs
	}

	// masters
	QVector<PatchNode*> ms;
	for (int mid : mids)	ms << masters[mid];

	// map from master to unit
	int unitIdx = ds->units.size();
	for (PatchNode* m : ms) masterUnitMap[m->mID] << unitIdx;

	// create
	UnitScaff* unit;
	QString id = QString::number(unitIdx);
	if (ms.size() == 2					// two masters
		&& ss.size() == 1				// one slave
		&& (ms[0]->hasTag(EDGE_VIRTUAL_TAG)
		|| ms[1]->hasTag(EDGE_VIRTUAL_TAG)))// one master is edge rod
	{
		unit = new TUnitScaff("TU_" + id, ms, ss, mPairs);
	}
	else if (!ParSingleton::instance()->inPlace // slanting is allowed
		&& ms.size() == 2				// two masters
		&& !ms[0]->hasTag(EDGE_VIRTUAL_TAG)
		&& !ms[1]->hasTag(EDGE_VIRTUAL_TAG) // both masters are real
		&& areParallel(ss))				// slaves are in parallel
	{
		unit = new ZUnitScaff("ZU_" + id, ms, ss, mPairs);
	}
	else
	{
		unit = new HUnitScaff("HU_" + id, ms, ss, mPairs);
	}

	return unit;
}

void Decomposer::computeUnitImportance()
{
	double totalA = 0;
	for (UnitScaff* u : ds->units)
		totalA += u->getTotalSlaveArea();

	for (UnitScaff* u : ds->units)
		u->setImportance(u->getTotalSlaveArea() / totalA);
}

bool Decomposer::areParallel(QVector<ScaffNode*>& ns)
{
	// all patch nodes
	QVector<PatchNode*> pns;
	for (ScaffNode* n : ns)
	if (n->mType == ScaffNode::PATCH)
		pns << (PatchNode*)n;

	// no patches
	if (pns.isEmpty()) return true;

	// consistency of patch normals
	bool parallel = true;
	Vector3 N0 = pns[0]->mPatch.Normal;
	for (int i = 0; i < pns.size(); i++)
	{
		Vector3 Ni = pns[i]->mPatch.Normal;
		double dotProd = dot(N0, Ni);
		if (fabs(dotProd) < 0.95)
		{
			parallel = false;
			break;
		}
	}

	return parallel;
}

void Decomposer::computeMasterOrderConstraints()
{
	// time stamps: bottom-up
	double tScale;
	Vector3 sqzV = ParSingleton::instance()->sqzV;
	QMap<QString, double> masterTimeStamps = getTimeStampsNormalized(masters, sqzV, tScale);

	// the base master is the bottom one that is not virtual
	double minT = maxDouble();
	for (PatchNode* m : masters){
		if (!m->hasTag(EDGE_VIRTUAL_TAG) && masterTimeStamps[m->mID] < minT){
			baseMaster = m;
			minT = masterTimeStamps[m->mID];
		}
	}

	// projected masters on the base master
	QMap<QString, Geom::Rectangle2> proj_rects;
	Geom::Rectangle base_rect = baseMaster->mPatch;
	for (PatchNode* m : masters)
	{
		// skip virtual 
		if (m->hasTag(EDGE_VIRTUAL_TAG)) continue;

		proj_rects[m->mID] = base_rect.get2DRectangle(m->mPatch);
	}

	// check each pair of masters
	for (int i = 0; i < masters.size(); i++)
	{
		// skip virtual 
		if (masters[i]->hasTag(EDGE_VIRTUAL_TAG)) continue;

		for (int j = i + 1; j < masters.size(); j++)
		{
			// skip virtual 
			if (masters[j]->hasTag(EDGE_VIRTUAL_TAG)) continue;

			// two non-virtual masters
			QString mi = masters[i]->mID;
			QString mj = masters[j]->mID;

			// test intersection between projections
			if (Geom::IntrRect2Rect2::test(proj_rects[mi], proj_rects[mj]))
			{
				double ti = masterTimeStamps[mi];
				double tj = masterTimeStamps[mj];

				// <up, down>
				if (ti > tj)
				{
					masterOrderGreater[mi] << mj;
					masterOrderLess[mj] << mi;
				}
				else
				{
					masterOrderGreater[mj] << mi;
					masterOrderLess[mi] << mj;
				}
			}
		}
	}
}


bool Decomposer::mergeInterlockUnits()
{
	// the map between old unit indices to the new ones
	QVector<int> newUnitIdx;
	QVector<UnitScaff*> oldUnits = ds->units;
	ds->units.clear();

	// delete interlocking/unfoldabilized units
	QVector<int> itlkUnitIdx;
	for (int i = 0; i < oldUnits.size(); i++){
		if (oldUnits[i]->mFoldDuration.start < 1){
			// units has been foldabilized
			ds->units << oldUnits[i];
			newUnitIdx << ds->units.size() - 1;
		}
		else{
			// interlocking units
			itlkUnitIdx << i;
			newUnitIdx << -1;
			delete oldUnits[i];
		}
	}

	// no interlocking
	if (itlkUnitIdx.isEmpty()) return false;

	// gather all interlocking slaves
	QSet<int> itlkSlaves;
	for (int ui : itlkUnitIdx){
		itlkSlaves += slaveClusters[ui];
		// map deleted unit to the new location
		newUnitIdx[ui] = ds->units.size();
	}

	// merge them as single unit to foldabilize
	ds->units << createUnit(itlkSlaves);

	// update master to unit map for key frame generation
	for (QString mid : masterUnitMap.keys()){
		foreach(int ui_old, masterUnitMap[mid]){
			int ui_new = newUnitIdx[ui_old];
			masterUnitMap[mid].remove(ui_old);
			masterUnitMap[mid].insert(ui_new);
		}
	}

	return true;
}
