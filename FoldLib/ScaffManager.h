#pragma once
#include <QObject>

#include "UtilityGlobal.h"
#include "Scaffold.h"
#include "FdUtility.h"

class ScaffManager final : public QObject
{
	Q_OBJECT

public:
    ScaffManager();
	~ScaffManager();

public:
	// input
	SurfaceMeshModel* wholeMesh;

	// scaffold
	Scaffold* scaffold;
	BOX_FIT_METHOD fitMethod;
	BOX_FIT_METHOD refitMethod;

public slots:
	// input
	void setMesh(Model* mesh);
	void setFitMethod(int method);
	void setRefitMethod(int method);

	// scaffold
	void createScaffold();
	void refitNodes();
	void changeNodeType();
	void saveScaffold();
	void loadScaffold();

signals:
	void scaffoldChanged(Scaffold* scaff);
	void contentChanged();
	void message(QString msg);
};


