#include "ParSingleton.h"

ParSingleton* ParSingleton::instance()
{
	static ParSingleton* _onlyInstance = nullptr;

	if (!_onlyInstance)
	{
		_onlyInstance = new ParSingleton();
	}

	return _onlyInstance;
}


ParSingleton::ParSingleton()
{
	restoreDefault();
}


void ParSingleton::restoreDefault()
{
	sqzV = Vector3(0, 0, 1);
	aabbCstrScale = Vector3(1.0, 1.0, 1.0);

	maxNbSplits = 1;
	maxNbChunks = 2;
	splitWeight = 0.5;

	connThrRatio = 0.07;

	nbKeyframes = 50;

	lookahead = true;

	useThk = true;
	usePlanA = true;
	usePlanB = false;

	inPlace = false;

	uniformH = false;

	// visual tags
	what2show = INITIAL_SCAFF;
	showAABB = false;
	showCuboid = true;
	showScaffold = true;
	showMesh = false;

	// require ui updating
	emit(defaultRestored());
}


void ParSingleton::setSqzV(QString sqzV_str)
{
	sqzV = Vector3(0, 0, 0);
	if (sqzV_str == "X") sqzV[0] = 1;
	if (sqzV_str == "Y") sqzV[1] = 1;
	if (sqzV_str == "Z") sqzV[2] = 1;
	if (sqzV_str == "-X") sqzV[0] = -1;
	if (sqzV_str == "-Y") sqzV[1] = -1;
	if (sqzV_str == "-Z") sqzV[2] = -1;

	emit(sqzVChanged());
}

void ParSingleton::setNbKeyframes(int N)
{
	nbKeyframes = N;
}

void ParSingleton::setNbSplits(int N)
{
	maxNbSplits = N;
}

void ParSingleton::setNbChunks(int N)
{
	maxNbChunks = N;
}

void ParSingleton::setConnThrRatio(double thr)
{
	connThrRatio = thr;
}

void ParSingleton::setAabbX(double x)
{
	aabbCstrScale[0] = x;
}

void ParSingleton::setAabbY(double y)
{
	aabbCstrScale[1] = y;
}

void ParSingleton::setAabbZ(double z)
{
	aabbCstrScale[2] = z;
}

void ParSingleton::setCostWeight(double w)
{
	splitWeight = w;
}

void ParSingleton::setShowAABB(int state)
{
	showAABB = (state == Qt::Checked);
	emit(visualOptionChanged());
}

void ParSingleton::setShowCuboid(int state)
{
	showCuboid = (state == Qt::Checked);
	emit(visualOptionChanged());
}

void ParSingleton::setShowScaffold(int state)
{
	showScaffold = (state == Qt::Checked);
	emit(visualOptionChanged());
}

void ParSingleton::setShowMesh(int state)
{
	showMesh = (state == Qt::Checked);
	emit(visualOptionChanged());
}

void ParSingleton::setUseThickness(int state)
{
	useThk = (state == Qt::Checked);
}

void ParSingleton::setUsePlanA(int state)
{
	usePlanA = (state == Qt::Checked);
}


void ParSingleton::setUsePlanB(int state)
{
	usePlanB = (state == Qt::Checked);
}

void ParSingleton::setUniformH(int state)
{
	uniformH = (state == Qt::Checked);
}


void ParSingleton::setInPlace(int state)
{
	inPlace = (state == Qt::Checked);

	emit(inPlaceChanged());
}

void ParSingleton::setLookahead(int state)
{
	lookahead = (state == Qt::Checked);
}


void ParSingleton::setWhat2Show(QString what)
{
	if (what == "Initial scaffold") what2show = INITIAL_SCAFF;
	if (what == "Configured scaffold") what2show = CONF_SCAFF;
	if (what == "Decomposition") what2show = DECOMP;
	if (what == "Keyframe") what2show = KEYFRAME;

	emit(visualOptionChanged());
}

void ParSingleton::setWhat2Show(WHAT2SHOW what)
{
	what2show = what;

	emit(what2ShowChanged());
}
