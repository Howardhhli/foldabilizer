#include "TChainScaff.h"
#include "Numeric.h"
#include "ParSingleton.h"

TChainScaff::TChainScaff(ScaffNode* slave, PatchNode* base, PatchNode* top)
:ChainScaff(slave, base, top)
{
}

Geom::Rectangle TChainScaff::getFoldRegion(FoldOption* fn)
{
	Geom::Rectangle base_rect = baseMaster->mPatch;

	double l = slaveSeg.length();
	double offset = l / (fn->nSplits + 1);

	Geom::Segment rightSeg, leftSeg;
	if (fn->rightSide)
	{
		leftSeg = baseJoint;
		rightSeg = baseJoint.translated(offset * rightDirect);
	}
	else
	{
		leftSeg = baseJoint.translated(-offset * rightDirect);
		rightSeg = baseJoint;
	}

	// shrink along jointV
	double t0 = fn->position;
	double t1 = t0 + fn->scale;
	leftSeg.cropRange01(t0, t1);
	rightSeg.cropRange01(t0, t1);

	// fold region in 3D
	Geom::Rectangle region(QVector<Vector3>()
		<< leftSeg.P0 << leftSeg.P1
		<< rightSeg.P1 << rightSeg.P0);

	return region;
}

void TChainScaff::computeCutPoints(FoldOption* fn)
{
	cutPoints.clear();

	// cut evenly
	double a = 1.0 / (fn->nSplits + 1);
	for (int i = 1; i <= fn->nSplits; i++)
		cutPoints << slaveSeg.getPosition01(i * a);
}

void TChainScaff::fold(double t)
{
	// free all nodes
	for (Structure::Node* n : nodes)
		n->removeTag(FIXED_NODE_TAG);

	// fix base
	baseMaster->addTag(FIXED_NODE_TAG);

	// set angle for active link
	double rootAngle = getRootAngle();
	activeLinks[0]->hinge->angle = rootAngle * (1 - t);
	for (int i = 1; i < activeLinks.size(); i++)
		activeLinks[i]->hinge->angle = M_PI * (1 - t);

	// restore configuration
	restoreConfiguration();
}

QVector<FoldOption*> TChainScaff::genRegularFoldOptions()
{
	// enumerate all possible combination of nS and nC
	QVector<FoldOption*> options;
	ParSingleton* ps = ParSingleton::instance();
	for (int nS = 0; nS <= ps->maxNbSplits; nS ++)
	for (int nC = 1; nC <= ps->maxNbChunks; nC++)
		options << ChainScaff::genRegularFoldOptions(nS, nC);

	return options;
}

double TChainScaff::getRootAngle()
{
	Vector3 V0 = slaveSeg.Direction;
	Vector3 V1 = foldToRight ? rightDirect : -rightDirect;
	return acos(RANGED(-1, dot(V0, V1), 1));
}